#!/bin/bash

set -a
set -e
set -x

default_pythonpath=$PYTHONPATH:.
export PYTHONPATH=${default_pythonpath:-.}

for f in `ls tests`; do
	if [ "test_" == ${f:0:5} ]; then
		python tests/$f
		if [ $? -ne 0 ]; then
			exit 1
		fi
	fi
done
set +x
set +e
set +a
