# standard imports
import unittest
import logging
import os

# test imports
from tests.base import TestBase
from taint.store.file import FileStore
from taint.taint import Tainter


class TestInterface(TestBase):

    def test_store(self):
        self.store = FileStore(self.session_data_dir)
        c = Tainter(self.chain_spec, self.bits_size, store=self.store)
        c.add_subject(self.alice)
        c.add_subject(self.bob)
        block_height = 42
        tx_index = 13
        c.add_tx(self.alice.account, self.bob.account, block_height, tx_index)
        c.save()

        stored = os.listdir(self.session_data_dir)
        self.assertEqual(len(stored), 3)
        self.assertIn(self.alice.account.hex(), stored)
        self.assertIn(self.bob.account.hex(), stored)
        self.assertIn(c.root_key().hex(), stored)
    
        c = Tainter(self.chain_spec, self.bits_size, store=self.store)
        r = c.load_subject(self.alice.account)
        self.assertTrue(r)
        r = c.load_object(self.bob.account)
        self.assertTrue(r)
       
        self.assertTrue(self.alice.is_same(c.subjects[self.alice.account]))
        self.assertTrue(self.bob.is_same(c.objects[self.bob.account]))
        self.assertFalse(c.have(block_height, tx_index))

        c = Tainter.load(self.store, self.chain_spec)
        self.assertTrue(c.have(block_height, tx_index))


if __name__ == '__main__':
    unittest.main()
