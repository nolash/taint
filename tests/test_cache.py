# standard imports
import unittest
import os
import copy
import logging

# external imports
from chainlib.chain import ChainSpec

# local imports
from taint.name import for_label
from taint.cache import (
        Cache,
        from_index,
        )
from taint.store.file import FileStore
from taint.account import Account

# test imports
from tests.base import TestBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestBasic(TestBase):

    def setUp(self):
        super(TestBasic, self).setUp()

        self.chain_spec = ChainSpec('foo', 'bar', 42, 'baz')
        self.account = os.urandom(20)

        self.filename = for_label(self.chain_spec, self.account, self.salt)
        self.filepath = os.path.join(self.session_data_dir, self.filename)
        self.store = FileStore(self.filepath)


    def test_divide(self):
        cache = Cache(self.chain_spec, self.bits_size, None)
        cache.add_subject(self.alice)
        cache.add_subject(self.bob)
        cache.add_object(self.eve)
        cache.add_object(self.mallory)
        someaccount = Account(self.chain_spec, os.urandom(20))

        (subjects, objects) = cache.divide([self.alice.account, self.bob.account, self.eve.account, self.mallory.account, someaccount.account])

        self.assertTrue(self.alice in subjects)
        self.assertFalse(self.alice in objects)
        
        self.assertTrue(self.bob in subjects)
        self.assertFalse(self.bob in objects)

        self.assertFalse(self.eve in subjects)
        self.assertTrue(self.eve in objects)

        self.assertFalse(self.mallory in subjects)
        self.assertTrue(self.mallory in objects)

        self.assertFalse(someaccount in subjects)
        self.assertFalse(someaccount in objects)

    
    def test_create_cache(self):
        cache = Cache(self.chain_spec, self.bits_size)
        cache.add_subject(self.alice)
        cache.add_object(self.bob)

        block_height = 42
        tx_index = 13

        self.assertNotEqual(self.alice.tags.get(), self.bob.tags.get())

        match = cache.add_tx(self.alice.account, self.bob.account, block_height, tx_index)
        self.assertNotEqual(match, None)

        self.assertEqual(self.alice.tags.get(), self.bob.tags.get())


    def test_state(self):
        cache = Cache(self.chain_spec, self.bits_size)

        cache.add_subject(self.alice)
        cache.add_subject(self.bob)
        cache.add_object(self.eve)

        first_block_height = 42
        first_tx_index = 13
        cache.add_tx(self.alice.account, self.bob.account, first_block_height, first_tx_index)

        new_block_height = 666
        new_tx_index = 1337
        cache.add_tx(self.alice.account, self.eve.account, new_block_height, new_tx_index)

        cache.first_block_height == first_block_height
        cache.first_tx_index == first_tx_index
        cache.last_block_height == new_block_height
        cache.last_tx_index == new_tx_index


    def test_recover(self):
        cache = Cache(self.chain_spec, self.bits_size)

        cache.add_subject(self.alice)
        cache.add_subject(self.bob)
        cache.add_object(self.eve)

        first_block_height = 42
        first_tx_index = 13
        cache.add_tx(self.alice.account, self.bob.account, first_block_height, first_tx_index)

        new_block_height = 666
        new_tx_index = 1337
        cache.add_tx(self.alice.account, self.eve.account, new_block_height, new_tx_index)

        self.assertTrue(cache.have(first_block_height, first_tx_index))
        self.assertTrue(cache.have(new_block_height, new_tx_index))

        cache = Cache(self.chain_spec, self.bits_size, cache_bloom=copy.copy(cache.cache_bloom))
        self.assertTrue(cache.have(first_block_height, first_tx_index))
        self.assertTrue(cache.have(new_block_height, new_tx_index))


    def test_serialize(self):
        cache = Cache(self.chain_spec, self.bits_size)

        cache.add_subject(self.alice)
        cache.add_subject(self.bob)
        cache.add_object(self.eve)

        first_block_height = 42
        first_tx_index = 13
        cache.add_tx(self.alice.account, self.bob.account, first_block_height, first_tx_index)

        new_block_height = 666
        new_tx_index = 1337
        cache.add_tx(self.alice.account, self.eve.account, new_block_height, new_tx_index)

        b = cache.serialize()
        cache_recovered = cache.from_serialized(self.chain_spec, b)
        self.assertEqual(cache_recovered.first_block_height, first_block_height)
        self.assertEqual(cache_recovered.first_tx_index, first_tx_index)


if __name__ == '__main__':
    unittest.main()
