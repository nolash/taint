# standard imports
import unittest
import logging

# local imports
from taint.tag import Tag

logging.basicConfig(level=logging.DEBUG)


class TestTag(unittest.TestCase):
    
    def test_tag_add(self):
        tag = Tag()
        self.assertEqual(tag.get(), b'\x00' * 32)

        a = tag.create(b'foo')
        b = tag.create(b'bar')
        self.assertNotEqual(a, b)

        self.assertFalse(tag.add(a))
        self.assertFalse(tag.add(b))
        
        z_one = tag.get()

        tag = Tag()
        tag.create(b'foo')
        tag.create(b'bar')

        z_two = tag.get()

        self.assertEqual(z_one, z_two)


    def test_tag_serialize(self):
        tag = Tag()

        tag.create(b'foo')
        tag.create(b'bar')

        s = tag.serialize()
        self.assertEqual(len(s), 32 * 3)

        tag_recovered = Tag()
        tag_recovered.deserialize(s)


if __name__ == '__main__':
    unittest.main()
