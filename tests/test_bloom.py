# standard imports
import os
import unittest
import copy

# local imports
from taint.cache import (
        CacheBloom,
        to_index,
        CacheAccountEnum,
        CacheStateEnum,
        )


class TestBloom(unittest.TestCase):

    def setUp(self):
        self.size = 1024
        self.bloom = CacheBloom(self.size)
        self.bloom.reset()
        self.alice = os.urandom(20)
        self.bob = os.urandom(20)

        self.bloom.add_raw(self.alice, CacheAccountEnum.SUBJECT)
        self.bloom.add_raw(self.bob, CacheAccountEnum.OBJECT)


    def reset_with_accounts(self):
        self.bloom.reset()
        self.bloom.add_raw(self.alice, CacheAccountEnum.SUBJECT)
        self.bloom.add_raw(self.bob, CacheAccountEnum.OBJECT)


    def test_bloom(self):

        orig_serial = self.bloom.serialize()

        self.bloom.add_raw(b'\x01', CacheAccountEnum.SUBJECT)
        self.bloom.add_raw(b'\x01', CacheAccountEnum.OBJECT)
        self.bloom.add_raw(b'\x01', CacheStateEnum.CACHE)
        self.bloom.add_raw(b'\x01', CacheStateEnum.EXTRA)

        b = self.bloom.serialize()
        byte_size = int(1024 / 8)
        self.assertNotEqual(orig_serial, self.bloom.serialize())

        self.reset_with_accounts()
        self.assertEqual(orig_serial, self.bloom.serialize())

        bloom_recovered = CacheBloom(self.size)
        self.assertNotEqual(b, bloom_recovered.serialize())

        bloom_recovered.deserialize(b)
        self.assertEqual(b, bloom_recovered.serialize())

        bloom_recovered = CacheBloom.from_serialized(b)
        self.assertEqual(b, bloom_recovered.serialize())
        

    def test_bloom_index(self):
        block_height = 42
        tx_index = 13
        index = to_index(42, 13)
        self.assertEqual(block_height, int.from_bytes(index[:12], 'big'))
        self.assertEqual(tx_index, int.from_bytes(index[12:], 'big'))


    def test_add(self):
        block_height = 42
        tx_index = 13

        orig_cache = copy.copy(self.bloom.filter['cache'].to_bytes())
        orig_extra = copy.copy(self.bloom.filter['extra'].to_bytes())

        r = self.bloom.register([self.alice, self.bob], block_height, tx_index)
        self.assertTrue(r)
        self.assertNotEqual(self.bloom.filter['cache'].to_bytes(), orig_cache)
        self.assertNotEqual(self.bloom.filter['extra'].to_bytes(), orig_extra)

        self.reset_with_accounts()
        r = self.bloom.register([self.alice], block_height, tx_index)
        self.assertTrue(r)
        self.assertNotEqual(self.bloom.filter['cache'].to_bytes(), orig_cache)
        self.assertEqual(self.bloom.filter['extra'].to_bytes(), orig_extra)

        self.reset_with_accounts()
        r = self.bloom.register([self.bob], block_height, tx_index)
        self.assertTrue(r)
        self.assertEqual(self.bloom.filter['cache'].to_bytes(), orig_cache)
        self.assertNotEqual(self.bloom.filter['extra'].to_bytes(), orig_extra)


    def test_check(self):
        block_height = 42
        tx_index = 13

        self.assertFalse(self.bloom.have_index(block_height, tx_index))
        
        r = self.bloom.register([self.alice], block_height, tx_index)
        self.assertTrue(self.bloom.have_index(block_height, tx_index))

        r = self.reset_with_accounts()
        r = self.bloom.register([self.bob], block_height, tx_index)
        self.assertTrue(self.bloom.have_index(block_height, tx_index))

        r = self.reset_with_accounts()
        someaccount = os.urandom(20)
        r = self.bloom.register([someaccount], block_height, tx_index)
        self.assertFalse(self.bloom.have_index(block_height, tx_index))


if __name__ == '__main__':
    unittest.main()
