# standard imports
import unittest
import copy

# local imports
from taint.account import Account

# test imports
from tests.base import TestBase


class TestAccount(TestBase):

    def test_account_compare(self):
        alice = self.alice
        alice_again = copy.copy(self.alice)
        self.assertTrue(alice.is_same(alice_again))

        alice_alias = Account(self.chain_spec, self.alice.account_src)
        self.assertTrue(alice.is_same(alice_alias))

        self.assertFalse(alice.is_same(self.bob))


    def test_connect_accounts(self):
        self.alice.connect(self.bob)
        self.assertEqual(self.alice.tags.get(), self.bob.tags.get())



    def test_serialize(self):
        self.alice.tags.create(b'xyzzy')
        z = self.alice.tags.get()

        b = self.alice.serialize()
        self.assertEqual(b[len(b)-32:], self.alice.account)

        new_alice = Account.from_serialized(b, self.chain_spec)
        self.assertTrue(new_alice.is_same(self.alice))


if __name__ == '__main__':
    unittest.main()
