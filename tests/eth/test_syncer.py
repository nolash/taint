# standard imports 
import unittest

# external imports
from chainlib.eth.gas import (
        Gas,
        RPCGasOracle,
    )
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.unittest.ethtester import EthTesterCase
from chainsyncer.driver import HistorySyncer
from chainsyncer.backend.memory import MemBackend
from hexathon import strip_0x

# local imports
from taint.taint import Tainter
from taint.account import Account
from taint.tag import (
        TagPool,
        Tag,
        )


class TestSyncer(EthTesterCase):

    def setUp(self):
        super(TestSyncer, self).setUp()
        self.syncer_backend = MemBackend(self.chain_spec, None, target_block=10)
        self.syncer = HistorySyncer(self.syncer_backend)

        self.alice_account = bytes.fromhex(strip_0x(self.accounts[0]))
        self.alice = Account(self.chain_spec, self.alice_account, label='alice')
        self.alice.tag(b'foo')

        self.bob_account = bytes.fromhex(strip_0x(self.accounts[1]))
        self.bob = Account(self.chain_spec, self.bob_account, label='bob')
        self.bob.tag(b'bar')

        self.filter_bits_size = 1024
        self.result_handler = TagPool()
        self.frontend = Tainter(self.chain_spec, self.filter_bits_size, result_handler=self.result_handler)
        self.frontend.add_subject(self.bob)
        self.frontend.add_subject(self.alice)

        self.syncer.add_filter(self.frontend)


    def test_sync_single(self):
        self.backend.mine_blocks(3)
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        gas_oracle = RPCGasOracle(self.rpc)
        c = Gas(self.chain_spec, gas_oracle=gas_oracle, nonce_oracle=nonce_oracle, signer=self.signer)
        (tx_hash_hex, o) = c.create(self.accounts[0], self.accounts[1], 1024)
        self.rpc.do(o)
        self.backend.mine_blocks(3)
        
        self.assertFalse(self.frontend.have(4, 0))

        self.syncer.loop(0.001, self.rpc)

        self.assertTrue(self.frontend.have(4, 0))

        tag = Tag()
        tag.create(b'foo')
        tag.create(b'bar')
        z = tag.get()

        self.assertEqual(len(self.result_handler.pool.keys()), 3)
        self.assertIn(z, self.result_handler.pool.keys())
        self.assertIn(self.alice, self.result_handler.pool[z])
        self.assertIn(self.bob, self.result_handler.pool[z])


if __name__ == '__main__':
    unittest.main()
