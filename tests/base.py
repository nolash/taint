# standard imports
import os
import unittest
import logging
import tempfile
import shutil

# external imports
from chainlib.chain import ChainSpec

# local imports
from taint.account import Account


BLOOM_BITS = 1024 * 1024 * 8

script_dir = os.path.realpath(os.path.dirname(__file__))
data_dir = os.path.join(script_dir, 'testdata')

logg = logging.getLogger().getChild(__name__)


class TestBase(unittest.TestCase):

    def setUp(self):
        os.makedirs(data_dir, exist_ok=True)
        self.salt = Account.salt
        self.session_data_dir = tempfile.mkdtemp(dir=data_dir)
        self.bits_size = BLOOM_BITS
        self.bytes_size = (BLOOM_BITS - 1) / 8 + 1

        self.chain_spec = ChainSpec('foo', 'bar', 42, 'baz')

        self.alice = Account(self.chain_spec, os.urandom(20), label='alice', tags=[b'inky'])
        self.bob = Account(self.chain_spec, os.urandom(20), label='bob', tags=[b'pinky'])
        self.eve = Account(self.chain_spec, os.urandom(20), label='eve', tags=[b'blinky'])
        self.mallory = Account(self.chain_spec, os.urandom(20), label='mallory', tags=[b'sue'])


    def tearDown(self):
        shutil.rmtree(self.session_data_dir)
