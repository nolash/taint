# standard imports
import logging
import sys

# external imports
from chainlib.chain import ChainSpec
from chainlib.eth.connection import EthHTTPConnection
from chainsyncer.backend.memory import MemBackend
from chainlib.eth.block import block_latest
from chainsyncer.driver import HistorySyncer

# local imports
from taint import Tainter
from taint.store.file import FileStore
from taint.account import Account
from taint.cache import CacheSyncBackend
from taint.sync.eth import EthCacheSyncer

logging.basicConfig(level=logging.INFO)
logging.getLogger('taint.cache').setLevel(logging.DEBUG)
logg = logging.getLogger()

store_dir = '/home/lash/tmp/storedir'

store = FileStore(store_dir)

chain_spec = ChainSpec('evm', 'foo', 42, 'bar')

ifc = Tainter(chain_spec, 8000, store=store)

rpc = EthHTTPConnection('http://localhost:8545')

#o = block_latest()
#stop = rpc.do(o)
start = 12423955
start -= 1
#stop = 12424184
stop = start
stop += 1
syncer_backend = MemBackend(chain_spec, None, target_block=stop)
syncer_backend.set(start, 0)


class MonitorFilter:

    def __init__(self, name='sync'):
        self.name = name


    def tick(self, block_number, tx_index):
        s = '{} sync block {} tx {}'.format(self.name, block_number, tx_index)
        sys.stdout.write('{:<100s}\r'.format(s))


    def filter(self, rpc, block, tx, session=None):
        self.tick(block.number, tx.index)


class MatchFilter:

    def filter(self, rpc, block, tx, session=None):
        logg.info('>>>>>>>>>>>>>>>>> block {} tx {}'.format(block, tx))



def block_monitor(block, Tx=None):
    s = 'sync block {} ({} txs)'.format(block.number, len(block.txs))
    sys.stdout.write('{:<100s}\r'.format(s))

syncer = HistorySyncer(syncer_backend, block_callback=block_monitor)

account = Account(chain_spec, bytes.fromhex('60c2fb18578665eb92636e7727e54e6b7c75f7ed'), tags=[b'foo'])
ifc.add_subject(account)

account = Account(chain_spec, bytes.fromhex('2bd15ebe0fac6831a9d12190a599385bee5515ad'), tags=[b'bar'])
ifc.add_object(account)

syncer.add_filter(MonitorFilter())
syncer.add_filter(ifc)

syncer.loop(0, rpc)

ifc.save()

for a in ifc.subjects.values():
    print(str(a))

for a in ifc.objects.values():
    print(str(a))

cache_backend = CacheSyncBackend(ifc, chain_spec, None, start_block=start, target_block=stop, tick_callback=MonitorFilter(name='cache').tick)
syncer = EthCacheSyncer(cache_backend)
syncer.add_filter(MatchFilter())
syncer.loop(0, rpc)
