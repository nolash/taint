# standard imports
import os

# local imports
from .tag import Tag
from .crypto import Salter


class Account(Salter):
    """Represents a single account in the cache.

    An account is a blockchain address associated with one or more tags. It provides methods to compare addresses, view tags, merge tags from two accounts, as well as serializing and deserializing for storage.

    The provided chain_spec will be used to generate the salt to obfuscate the address in the cache.

    :param chain_spec: The chain spec the address is valid for
    :type chain_spec: chainlib.chain.ChainSpec
    :param account: The account address
    :type account: bytes
    :param label: Human-readable label for account used for logging
    :type label: str
    :param tags: Tags to associate account with 
    :type tags: list of bytes
    :param create_digest: If set to false, account obfuscation will be omitted
    :type create_digest: boolean
    """
    def __init__(self, chain_spec, account, label=None, tags=[], create_digest=True):
        super(Account, self).__init__(chain_spec)

        if label == None:
            label = str(account)
        self.label = label
        self.account_src = None
        self.create_digest = create_digest
        if self.create_digest:
            self.account_src = account
            self.account = self.sprinkle(self.account_src)
        else:
            self.account = account
        self.tags = Tag()
        for tag in tags:
            self.tags.create(tag)


    def tag(self, value):
        """Add a tag to the account.

        :param value: Literal tag value
        :type value: bytes
        """
        self.tags.create(value)


    def sum(self):
        """Get the sum of all the tags for the account.

        :rtype: bytes
        :returns: Tag sum
        """
        return self.tags.get()


    def connect(self, account):
        """Associate two accounts with each other. After this operation, both accounts will have the same tag sum.

        :param account: Account to merge with
        :type account: taint.account.Account
        """
        if not isinstance(account, Account):
            raise TypeError('account must be type taint.account.Account')
        self.tags.merge(account.tags)


    def is_same(self, account):
        """Compare two accounts.

        This will not compare the tag state of the accounts.

        :param account: Account to compare
        :type account: taint.account.Account
        :rtype: boolean
        :return: True if the account effectively represents the same underlying blockchain address
        """
        if not isinstance(account, Account):
            raise TypeError('account must be type crypto_account_cache.account.Account')
        return self.account == account.account


#    def __eq__(self, account):
#       return self.is_same(account)


    def is_account(self, address):
        """Compare blockchain address to address represented by account object.

        If account obfuscation is being used, the input value has to match the unobfuscated value.

        :param address: Address to compare with
        :type address: bytes
        :rtype: boolean
        :return: True on address match
        """
        if self.create_digest:
            return self.sprinkle(address) == self.account
        return address == self.account


    def serialize(self):
        """Serialize account object for storage.

        Account serialization consists of serialization of the account's tags, followed by the serialization of the underlying blockchain address.

        :rtype: bytes
        :return: Serialized data
        """
        b = self.tags.serialize() + self.account
        return b


    @staticmethod
    def from_serialized(b, chain_spec, label=None):
        """Deserialize account object from storage.

        BUG: deserialization may break if account is not obfuscated, since the address may not end on 32 byte boundary

        :param chain_spec: Chain spec to instantiate account for
        :type chain_spec: chainlib.chain.ChainSpec
        :param label: Human-readable label for logging
        :type label: str
        :rtype: taint.account.Account
        :returns: Deserialized account
        """
        l = len(b)
        if l % 32 > 0:
            raise ValueError('invalid data length; remainder {} of 32'.format(l % 32))
        if l < 64: 
            raise ValueError('invalid data length; expected minimum 64, got {}'.format(l))

        a = Account(chain_spec, b[-32:], label=label, create_digest=False)
        a.tags.deserialize(b[:-32])
        return a


    def __str__(self):
        return '{} [{}]'.format(self.account.hex(), str(self.tags))
