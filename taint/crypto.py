# standard imports
import hashlib
import os


class Salter:
    """Base class to provide cryptographic salt for cache objects that should be obfuscated.
    
    By default a random base value will be generated. The salt will be deterministically determined from the value and the provided chain spec.

    :param chain_spec: Chain spec to generate the salt with.
    :type chain_spec: chainlib.chain.ChainSpec
    """
    salt = os.urandom(32)

    def __init__(self, chain_spec):
        self.chain_spec = chain_spec
        self.ionized_salt = self.salt
        self.ionized_salt = self.sprinkle(str(chain_spec).encode('utf-8'))


    def sprinkle(self, data):
        """Hash the given data with the salt

        :param data: Input data
        :type data: bytes
        :rtype: bytes
        :returns: Hashed, salted value

        """
        h = hashlib.new('sha256')
        if isinstance(data, list):
            for d in data:
                h.update(d)
        else:
            h.update(data)
        h.update(self.ionized_salt)
        return h.digest()


    def root_key(self):
        """Returns the salt value generated from the chain spec.

        :rtype: bytes
        :returns: Salt
        """
        return self.ionized_salt
