# standard imports
import os

# local imports
from .base import (
        to_key,
        BaseStore,
        )


class FileStore(BaseStore):
    """Filesystem backend for storing key value pairs with filenames as keys.

    Base storage directory will be created if it does not exist.

    :param base_dir: Base storage directory
    :type base_dir: str
    """

    def __init__(self, base_dir):
        os.makedirs(base_dir, exist_ok=True)
        self.base_dir = base_dir 


    def put(self, k, v):
        """Implements taint.store.base.BaseStore
        """
        k = to_key(k)
        filepath = os.path.join(self.base_dir, k)

        f = open(filepath, 'wb')
   
        l = len(v)
        c = 0
        while c < l:
            c += f.write(v[c:])
        f.close()


    def get(self, k):
        """Implements taint.store.base.BaseStore
        """
        k = to_key(k)
        filepath = os.path.join(self.base_dir, k)

        f = open(filepath, 'rb')

        b = b''
        c = -1
        while c != 0:
            d = f.read(4096)
            c = len(d)
            b += d

        f.close()

        return b
