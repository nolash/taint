# external imports
from hexathon import even


def to_key(k):
    """Ensure even-numbered hex format for given storage key.

    :param k: Key
    :type k: bytes, bytearray or str (hex)
    :rtype: str
    :returns: Even-numbered hex represenation of key
    """
    if isinstance(k, bytes) or isinstance(k, bytearray):
        k = k.hex()
    else:
        k = even(k)
    return k


class BaseStore:

    def put(self, k, v):
        """Store value v under key k

        :param k: Key
        :type k: bytes
        :param v: Value
        :type v: bytes
        """
        raise NotImplementedError


    def get(self, k):
        """Return value stored under key k

        :param k: Key
        :type k: bytes
        :rtype: bytes
        :return: Value
        """
        raise NotImplementedError
