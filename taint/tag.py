# standard imports
import hashlib
import logging

logg = logging.getLogger().getChild(__name__)



class TagPool:

    def __init__(self):
        self.pool = {}


    def register(self, account):
        z = account.sum()
        if self.pool.get(z) == None:
            self.pool[z] = []

        self.pool[z].append(account)
        
        logg.debug('pool register account {} to tag sum {}'.format(account, z.hex()))

        return True


class Tag:
    """Represents a collection of tags for a cached object.

    When a new tag is added, the tag collection is deterministically ordered and summed.
    """
    def __init__(self):
        self.tags = []
        self.tag_values = {}
        self.sum = b'\x00' * 32
        self.dirty = False
               

    def get(self):
        """The current deterministic sum of the tags.

        :rtype: bytes
        :return: Tag digest sum
        """
        if self.dirty:
            self.tags.sort()
            h = hashlib.new('sha256') 
            for tag in self.tags:
                h.update(tag)
            self.sum = h.digest()
        return self.sum


    def add(self, tag, value=None):
        """Add a tag to the collection.

        Client code should call Tag.create() instead.

        :param tag: Tag value digest
        :type tag: bytes
        :param value: Tag value
        :type value: bytes
        :rtype: boolean
        :returns: False if tag digest already exists in object
        """
        if tag in self.tags:
            return False
        self.tags.append(tag)
        self.tag_values[tag] = value
        self.dirty = True
        return True


    def create(self, value):
        """Create a new tag record to add to the collection.

        :param value: Tag value
        :type value: bytes
        :rtype: bytes
        :return: Digest of newly added tag
        """
        h = hashlib.new('sha256')
        h.update(value)
        tag = h.digest()
        self.add(tag, value)
        return tag


    def merge(self, tags):
        """Merge contents of two tag objects. After this operation the sum of each of the tag objects will be identical.

        :param tags: Tag collection to merge with
        :type tags: taint.tag.Tag
        :raises TypeError: If argument is not a taint.tag.Tag instance
        """
        if not isinstance(tags, Tag):
            raise TypeError('tags must be type taint.tag.Tag')
        for tag in tags.tags:
            self.add(tag)
            self.tag_values[tag] = tags.tag_values[tag]

        for tag in self.tags:
            tags.add(tag)
            tags.tag_values[tag] = self.tag_values[tag]


    def serialize(self):
        """Serialize tags for storage.

        Serialized tags are deterministically ordered.

        :rtype: bytes
        :returns: Serialized tags
        """
        b = self.get()
        for tag in self.tags:
            b += tag
        return b


    def deserialize(self, b, skip_check=False):
        """Deserialize tags into currently instantiated object.

        Deserialization will ADD tags to the current object. If different tags already exist in the object, the resulting collection will not be identical to the serialized data.

        :param b: Serialized tag data
        :type b: bytes
        :raises ValueError: If skip_check is not set, and serialized data does not match tag object sum
        """
        if len(b) % 32 > 0:
            raise ValueError('invalid data length; remainder {} from 32'.format(len(b) % 32))
        cursor = 32
        z = b[:cursor]

        for i in range(cursor, len(b), 32):
            tag = b[i:i+32]
            logg.debug('deserialize add {}'.format(tag))
            self.add(tag)

        if not skip_check:
            zz = self.get()
            if z != zz:
                raise ValueError('data sum does not match content; expected {}, found {}'.format(zz.hex(), z.hex()))


    def __str__(self):
        tag_list = []
        for tag in self.tags:
            v = self.tag_values[tag]
            if v == None:
                v = tag.hex()
            else:
                try:
                    v = v.decode('utf-8')
                except UnicodeDecodeError:
                    v = v.hex()
            tag_list.append(v)
        tag_list.sort()
        return ','.join(tag_list)
