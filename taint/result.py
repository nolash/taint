# standard imports
import logging

logg = logging.getLogger().getChild(__name__)


class Result:

    def __init__(self):
        self.handler = []


    def add_handler(self, result_handler):
        self.handlers.append(result_handler)
        logg.debug('added handler {}'.format(str(result_handler)))


    def register(self, account):
        for h in self.handlers:
            r = h(account)
            logg.debug('processed handler {} -> {}'.format(str(h), r))
