# standard imports
import uuid
import logging

# external imports
from chainsyncer.driver import HistorySyncer
from chainlib.eth.tx import (
        transaction,
        receipt,
        Tx,
        )

logg = logging.getLogger(__name__)


class EthCacheSyncer(HistorySyncer):

    def process(self, conn, block):
        (pair, fltr) = self.backend.get()
        try:
            tx = block.tx(pair[1])
        except AttributeError:
            o = transaction(block.txs[pair[1]])
            r = conn.do(o)
            tx = Tx(Tx.src_normalize(r), block=block)
        except IndexError as e:
            logg.debug('index error syncer rcpt get {}'.format(e))
            self.backend.set(block.number + 1, 0)
            return

        # TODO: Move specifics to eth subpackage, receipts are not a global concept
        rcpt = conn.do(receipt(tx.hash))
        if rcpt != None:
            tx.apply_receipt(Tx.src_normalize(rcpt))

        self.process_single(conn, block, tx)

        self.backend.set(pair[0], pair[1] + 1)
