# standard imports
import logging

# external imports
from hexathon import strip_0x

# local imports
from .cache import Cache
from .account import Account
from .crypto import Salter

logg = logging.getLogger().getChild(__name__)


def noop_process_actors(self, inputs, outputs):
    return (inputs, outputs,)


class Tainter(Cache):
    """Frontend object containing code to load and save state of a cache, aswell as chain sync handling.

    :param chain_spec: Chain spec context for the cache
    :type chain_spec: chainlib.chain.ChainSpec
    :param bits_size: Bitsize of bloom filter used for cache
    :type bits_size: int
    :param result_handler: Callback called once for each registered account found in a transaction.
    :type result_handler: function
    :param store: State storage for cache
    :type store: taint.store.base.BaseStore
    :param cache_bloom: Cache bloom filter to instantiate
    :type cache_bloom: taint.cache.CacheBloom
    """
    def __init__(self, chain_spec, bits_size, result_handler=None, store=None, cache_bloom=None):
        super(Tainter, self).__init__(chain_spec, bits_size, cache_bloom=cache_bloom)
        self.store = store
        self.result_handler = result_handler
        self.processors = [noop_process_actors]


    def register_actor_processor(self, processor):
        self.processors.insert(0, processor)


    def add_account(self, account, label):
        """Add account to be tracked in cache.

        If registered, the result handler will be called with the initial state of the added account.

        The label will only be stored in memory for the given session, and will not be part of state storage.

        :param account: Account to add
        :type account: taint.account.Account
        :param label: Filter section to add account to
        :type label: taint.cache.CacheAccountEnum
        """
        super(Tainter, self).add_account(account, label)
        if self.result_handler != None:
            self.result_handler.register(account)


    def filter(self, conn, block, tx, storer):
        """Transaction callback for chainsyncer.

        :param conn: RPC connection object
        :type conn: chainlib.connection.RPCConnection
        :param block: Block object
        :type block: chainlib.block.Block
        :param tx: Transaction object
        :type tx: chainlib.tx.Tx
        :param storer: State storage object (e.g. a sql database session)
        :type storer: any
        """
        outputs = None
        inputs = None

        for p in self.processors:
            r = p(tx.outputs, tx.inputs)
            if r != None:
                outputs = r[0]
                inputs = r[1]
                break

        for output in outputs:
            for inpt in inputs:
                sender = bytes.fromhex(strip_0x(output))
                recipient = bytes.fromhex(strip_0x(inpt))
                r = None
                try:
                    r = self.add_tx(
                            self.sprinkle(sender),
                            self.sprinkle(recipient),
                            block.number,
                            tx.index,
                            block_hash=block.hash,
                            tx_hash=tx.hash,
                            )
                except KeyError:
                    logg.debug('false positive match tx {}'.format(tx.hash))
                    continue

                if r == None:
                    continue

                subjects = r[0]
                objects = r[1]
                
                if self.result_handler != None:
                    for account in subjects:
                        self.result_handler.register(account)
                    for account in objects:
                        self.result_handler.register(account)


    def save(self):
        """Save state of all accounts and the salt used for the session to the cache store.
        """
        for account in self.subjects.values():
            self.store.put(account.account, account.serialize())
        self.store.put(self.root_key(), self.serialize())


    def load_account(self, k, label=None):
        """Load state for an accounts from a cache store.

        :param k: Account to load, by obfuscated value.
        :type k: bytes
        :param label: Label to associate with account, for display use.
        """
        try:
            b = self.store.get(k)
        except FileNotFoundError:
            return None
        return Account.from_serialized(b, self.chain_spec, label)


    def load_subject(self, k, label=None):
        """Load state for an account as subject from the cache store.

        A subject will always merge tags with any other subject or object in the same transaction.

        :param k: Account to load, by obfuscated value.
        :type k: bytes
        :param label: Label to associate with account, for display use.
        """
        a = self.load_account(k, label)
        if a == None:
            return False
        self.add_subject(a)
        return True


    def load_object(self, k, label=None):
        """Load state for an account as object from the cache store.

        An object will only merge tags with other subjects in the same transaction.

        :param k: Account to load, by obfuscated value.
        :type k: bytes
        :param label: Label to associate with account, for display use.
        """
        a = self.load_account(k, label)
        if a == None:
            return False
        self.add_object(a)
        return True


    @staticmethod
    def load(store, chain_spec, result_handler=None):
        """Instantiate new Tainter object with salt stored from previous session.
        """
        a = Salter(chain_spec)
        b = store.get(a.root_key())
        c = Tainter.from_serialized(chain_spec, b)
        c.store = store
        c.result_handler = result_handler
        return c

