# standard imports
import os
import logging
import enum

# external imports
from moolb import Bloom
from chainsyncer.backend.memory import MemBackend

# local imports
from .name import for_label
from .crypto import Salter
from .account import Account

logg = logging.getLogger().getChild(__name__)


class CacheAccountEnum(enum.Enum):
    SUBJECT = 'subject'
    OBJECT = 'object'


class CacheStateEnum(enum.Enum):
    CACHE = 'cache'
    EXTRA = 'extra'


def to_index(block_height, tx_index=None):
    """Create a cache store serialized index from block height and transaction index
    """
    b = block_height.to_bytes(12, 'big')
    if tx_index != None:
        b += tx_index.to_bytes(4, 'big')
    return b


def from_index(b):
    """Load ablock height and transaction index from a cache store serialized index
    """
    block_height = int.from_bytes(b[:12], 'big')
    tx_index = int.from_bytes(b[12:], 'big')
    return (block_height, tx_index)


class CacheBloom:
    """Bloom filter for a cache state.

    The filter has four parts, all identified by the values of the taint.cache.CacheAccountEnum and taint.cache.CacheStateEnum classes:

    - subject: All subject account addresses being tracked
    - object: All object account addresses being tracked
    - cache: All block/tx indexes involving a subject address
    - extra: All block/tx indexes involving an object address

    Filter values are calculated using sha256 (the default of the underlying "moolb" python module)

    :param bits_size: Bit size of bloom filter
    :type bits_size: int
    """

    rounds = 3
    """Number of hashing rounds used to calculate a single cache entry"""
    
    def __init__(self, bits_size):
        self.bits_size = bits_size
        self.filter = {}
        for v in CacheAccountEnum:
            self.filter[v.value] = None
        for v in CacheStateEnum:
            self.filter[v.value] = None


    def reset(self):
        """Empties all filters.
        """
        for v in CacheAccountEnum:
            self.filter[v.value] = Bloom(self.bits_size, CacheBloom.rounds)
        for v in CacheStateEnum:
            self.filter[v.value] = Bloom(self.bits_size, CacheBloom.rounds)


    def add_raw(self, v, label):
        """Add a raw byte value to the bloom filter part with the corresponding label.

        :param v: Value to add
        :type v: bytes
        :param label: Filter section to add value to
        :type label: CacheAccountEnum or CacheStateEnum
        """
        self.filter[label.value].add(v)


    def serialize(self):
        """Serialize cache bloom filter state for storage.

        The serialized format of the filter is simply all filter contents concatenated in the following order:

        1. subject
        2. object
        3. cache
        4. extra

        :rtype: bytes
        :returns: Serialized cache state
        """
        if self.filter[CacheAccountEnum.SUBJECT.value] == None:
            logg.warning('serialize called on uninitialized cache bloom')
            return b''

        b = b''
        for v in CacheAccountEnum:
            b += self.filter[v.value].to_bytes()
        for v in CacheStateEnum:
            b += self.filter[v.value].to_bytes()

        return b


    def deserialize(self, b):
        """Deserialize a stored cache bloom filter state into instantiated BloomCache object.

        Any existing bloom filter state in the object will be overwritten.

        Client code should use static method taint.cache.BloomCache.from_serialized() instead.

        :param b: Serialized bloom filter state
        :type b: bytes
        """
        byte_size = int(self.bits_size / 8)
        length_expect = byte_size * 4
        length_data = len(b)
        if length_data != length_expect:
            raise ValueError('data size mismatch; expected {}, got {}'.format(length_expect, length_data))

        cursor = 0
        for v in CacheAccountEnum:
            self.filter[v.value] = Bloom(self.bits_size, CacheBloom.rounds, default_data=b[cursor:cursor+byte_size])
            cursor += byte_size

        for v in CacheStateEnum:
            self.filter[v.value] = Bloom(self.bits_size, CacheBloom.rounds, default_data=b[cursor:cursor+byte_size])
            cursor += byte_size


    @staticmethod
    def from_serialized(b):
        """Convenience function to deserialize a stored cache bloom filter state.

        :param b: Serialized bloom filter state
        :type b: bytes
        :raises ValueError: If data does not pass integrity check
        :rtype: taint.cache.BloomCache
        :returns: Instantiated bloom cache objectcache object
        """
        if len(b) % 4 > 0:
            raise ValueError('invalid data length, remainder {} of 4'.format(len(b) % 32))

        bits_size = int((len(b) * 8) / 4)
        bloom = CacheBloom(bits_size)
        bloom.deserialize(b)
        return bloom


    def have(self, data, label):
        """Check if value generates a match in bloom filter

        :param data: Data to match
        :type data: byts
        :param label: Bloom cache section to match
        :type label: CacheAccountEnum or CacheStateEnum
        """
        return self.filter[label.value].check(data)


    def have_index(self, block_height, tx_index=None):
        """Check if block number or block/tx index exists in bloom cache.

        This will match against any of the 'cache' and 'extra' sections.

        :param block_height: Block height to match
        :type block_height: int
        :param tx_index: Transaction index to match (optional)
        :type tx_index: int
        :rtype: boolean
        :return: True if bloom filter match in one of the sections
        """
        b = to_index(block_height, tx_index)
        if self.have(b, CacheStateEnum.CACHE):
            return True
        return self.have(b, CacheStateEnum.EXTRA)


    def register(self, accounts, block_height, tx_index=None):
        """Add a match for block number or block/tx index for the specified accounts.

        If none of the given accounts exist in the tracked account filter, no change will be made to state. 

        BUG: False positive accounts matches are not discarded.

        :param accounts: List of blockchain addresses to match
        :type accounts: list of bytes
        :param block_height: Block height to register
        :type block_height: int
        :param tx_index: Transaction index to register
        :type tx_index: int
        :rtype: boolean
        :return: False if no match in accounts was found.
        """
        subject_match = False
        object_match = False
        for account in accounts:
            if self.have(account, CacheAccountEnum.SUBJECT):
                subject_match = True
            elif self.have(account, CacheAccountEnum.OBJECT):
                object_match = True

        if not subject_match and not object_match:
            return False

        b = to_index(block_height, tx_index)
        if subject_match:
            self.add_raw(b, CacheStateEnum.CACHE)
        if object_match:
            self.add_raw(b, CacheStateEnum.EXTRA)

        return True


class Cache(Salter):
    """Core session engine for caching and associating block transactions with accounts.

    If cache_bloom is omitted, a new CacheBloom object will be instantiated as backend, using the provided bits_size.

    :param chain_spec: Chain spec to use cache for.
    :type chain_spec: chainlib.chain.ChainSpec
    :param bits_size: Bit size of underlying bloomfilter
    :type bits_size: int
    :param cache_bloom: Cache bloom state to initialize cache session with
    :type cache_bloom: taint.cache.CacheBloom
    """
    def __init__(self, chain_spec, bits_size, cache_bloom=None):
        super(Cache, self).__init__(chain_spec)
        self.bits_size = bits_size
        self.chain_spec = chain_spec

        if cache_bloom == None:
            cache_bloom = CacheBloom(bits_size)
            cache_bloom.reset()

        self.cache_bloom = cache_bloom
        self.subjects = {}
        self.objects = {}

        self.first_block_height = -1
        self.first_tx_index = 0
        self.last_block_height = 0
        self.last_tx_index = 0


    def serialize(self):
        """Serialize the underlying bloom cache state together with the block range of registered matches.

        :raises AttributeError: If no content has yet been cached
        :rtype: bytes
        :return: Serialized cache state
        """
        if self.first_block_height < 0:
            raise AttributeError('no content to serialize')

        b = to_index(self.first_block_height, self.first_tx_index)
        b += to_index(self.last_block_height, self.last_tx_index)
        bb = self.cache_bloom.serialize()
        return bb + b


    @classmethod
    def from_serialized(cls, chain_spec, b):
        """Instantiate a new Cache object from a previously serialized state.

        :param chain_spec: Chain spec to instantiate the Cache object for
        :type chain_spec: chainlib.chain.ChainSpec
        :param b: Serialized data
        :type b: bytes
        :rtype: taint.cache.Cache
        :return: Instantiated cache object
        """
        cursor = len(b)-32
        bloom = CacheBloom.from_serialized(b[:cursor])
        c = cls(chain_spec, bloom.bits_size, cache_bloom=bloom)

        (c.first_block_height, c.first_tx_index) = from_index(b[cursor:cursor+16])
        cursor += 16
        (c.last_block_height, c.last_tx_index) = from_index(b[cursor:cursor+16])

        return c


    def divide(self, accounts):
        """Divides the given accounts into subjects and objects depending on their match in the bloom cache state backend.

        Accounts that do not generate matches will be omitted.

        :param accounts: List of blockchain addresses to process
        :type account: List of bytes
        :rtype: tuple of lists of bytes
        :return: list of subjects and list of objects, in that order
        """
        subjects = []
        objects = []

        for account in accounts:
            if self.cache_bloom.have(account, CacheAccountEnum.SUBJECT):
                subject = self.subjects[account]
                subjects.append(subject)
            elif self.cache_bloom.have(account, CacheAccountEnum.OBJECT):
                objct = self.objects[account]
                objects.append(objct)

        return (subjects, objects)


    def add_account(self, account, label):
        """Add a new account to the bloom cache state, in the corresponding section

        Client code should use taint.cache.Cache.add_subject() or taint.cache.Cache.add_object() instead.

        :param account: account to add
        :type account: taint.account.Account
        :param label: bloom cache section
        :type label: taint.cache.CacheAccountEnum
        """
        self.cache_bloom.add_raw(account.account, label)


    def add_subject(self, account):
        """Convenience function to add an account as a subject.

        :param account: account to add
        :type account: taint.account.Account
        :raises TypeError: If account is not right type
        """
        if not isinstance(account, Account):
            raise TypeError('subject must be type taint.account.Account')
        self.add_account(account, CacheAccountEnum.SUBJECT)
        logg.debug('added subject {}'.format(account))
        self.subjects[account.account] = account


    def add_object(self, account):
        """Convenience function to add an account as a object.

        :param account: account to add
        :type account: taint.account.Account
        :raises TypeError: If account is not right type
        """

        if not isinstance(account, Account):
            raise TypeError('subject must be type taint.account.Account')
        self.add_account(account, CacheAccountEnum.OBJECT)
        logg.debug('added object {}'.format(account))
        self.objects[account.account] = account


    def add_tx(self, sender, recipient, block_height, tx_index, block_hash=None, tx_hash=None, relays=[]):
        """Add a transaction to the bloom cache state.

        If a subject address is matched, tags will be merged for all subjects involved in the transaction.

        If an object address is matched, tags will be merged for all subjects and the object involved in the transaction.

        :param sender: Blockchain addresses providing output for the transaction
        :type sender: list of bytes
        :param recipient: Blockchain addresses providing input for the transaction
        :type recipient: list of bytes
        :param block_height: Block height of transaction
        :type block_height: int
        :param tx_index: Transaction index in block
        :type tx_index: int
        :param block_hash: Block hash (used for debugging/log output only)
        :type block_hash: str
        :param tx_hash: Transaction hash (used for debugging/log output only)
        :type tx_hash: str
        :param relays: Additional blockchain addresses to generate match for
        :type relays: list of bytes
        :rtype: tuple of lists of bytes
        :return: Matched subjects and objects, or None of no matching account was found
        """
        accounts = [sender, recipient] + relays
        self.cache_bloom.register(accounts, block_height)
        match = self.cache_bloom.register(accounts, block_height, tx_index)

        if not match:
            return None

        if self.first_block_height == -1:
            self.first_block_height = block_height
            self.first_tx_index = tx_index
        self.last_block_height = block_height
        self.last_tx_index = tx_index

        logg.info('match in {}:{} {}:{}'.format(block_height, tx_index, block_hash, tx_hash))

        # TODO: watch out, this currently scales geometrically
        (subjects, objects) = self.divide(accounts)
        logg.debug('subjects {} objects {}'.format(subjects, objects))
        for subject in subjects:
            for objct in objects:
                subject.connect(objct) 
            for other_subject in subjects:
                if subject.is_same(other_subject):
                    continue
                subject.connect(other_subject)

        return (subjects, objects)


    def have(self, block_height, tx_index=None):
        """Check if block number or block/tx index exists in bloom cache state

        :param block_height: Block height to match
        :type block_height: int
        :param tx_index: Transaction index to match
        :type tx_index: int
        :rtype: boolean
        :return: True on match
        """
        return self.cache_bloom.have_index(block_height, tx_index)


class CacheSyncBackend(MemBackend):
    """Volatile chainsyncer backend generating matches for all block/tx matched in the bloom cache state.

    Can be used to replay the syncing session for only the block/tx indices known to be of interest.
    
    TODO: Add a tx_index max value hint on stored blocks to eliminate the need for the scan_limit, which can cause transactions to be missed, aswell as reduce resource usage.

    :param cache: Cache object
    :type cache taint.cache.Cache
    :param chain_spec: Chain spec to run the syncer session for
    :type chain_spec: chainlib.chain.ChainSpec
    :param object_id: chainsyncer backend object id
    :type object_id: str
    :param start_block: Block offset to start syncing at, inclusive
    :type start_block: int
    :param target_block: Block to stop syncing at, exclusive
    :type target_block: int
    :param tick_callback: Callback called for every processed transaction
    :type tick_callback: function receiving block_height and tx_index
    :param tx_scan_limit: Maximum transaction index in a block to scan for
    :type tx_scan_limit: int
    """
    def __init__(self, cache, chain_spec, object_id, start_block=0, target_block=0, tick_callback=None, tx_scan_limit=500):
        if target_block <= start_block:
            raise ValueError('target block number must be higher than start block number')
        super(CacheSyncBackend, self).__init__(chain_spec, object_id, target_block)
        self.cache = cache
        self.set(start_block, 0)
        self.tick_callback = tick_callback
        self.tx_scan_limit = tx_scan_limit


    def get(self):
        """Advance to the next matched block/tx index in the bloom cache state, and return as a block index result for the chainsyncer sync driver.

        Transaction execution filters for the syncer are not implemented, so the returned filter state will always be 0.

        :rtype: tuple
        :return: tuple of block_height and tx_index, and a static 0 as filter value
        """
        while self.block_height < self.target_block + 1:
            if self.cache.have(self.block_height):
                if self.tx_height < self.tx_scan_limit:
                    if self.tick_callback != None:
                        self.tick_callback(self.block_height, self.tx_height)
                    if self.cache.have(self.block_height, self.tx_height):
                        return ((self.block_height, self.tx_height), 0)
                    self.tx_height += 1
                    continue
            else:
                if self.tick_callback != None:
                    self.tick_callback(self.block_height, self.tx_height)
            self.block_height += 1
            self.tx_height = 0
        return ((self.block_height, self.tx_height), 0)
